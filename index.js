const Telegraf = require('telegraf')

function sendLiveLocation (ctx) {
  let lat = 42.0
  let lon = 42.0
  ctx.replyWithLocation(lat, lon, { live_period: 60 }).then((message) => {

    console.log(message);
    const timer = setInterval(() => {
      lat += Math.random() * 0.001
      lon += Math.random() * 0.001
      ctx.telegram.editMessageLiveLocation(lat, lon, message.chat.id, message.message_id).catch(() => clearInterval(timer))
    }, 1000)
  })
}

const app = new Telegraf('463987307:AAHwXEEgeY7MQA7AFUqp2h_rwXFdXZa7PwU')
app.command('start', ({ from, reply }) => {
  console.log('start', from)
  return reply('Welcome!')
})
app.hears('hi', (ctx) => ctx.reply('Hey there!'))
app.on('sticker', (ctx) => ctx.reply('👍'))

app.command('/start', sendLiveLocation)
app.startPolling()
